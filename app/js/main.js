(function() {
	var dStart = new Date(),
		doc = $(document),
		win = $(window),
		scrollT = win.scrollTop(),
		dataAttr,
		headerH, footerH, dEnd, totalD;

	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();
	function clearClasses() {
		if ( win.width() > 768 ) {
			$('[data-id="' + dataAttr + '"]').removeClass('visible');
		}
	}
	function mobileFullPageToggle() {
		$('[data-open]').click(function() {
			$('body').css({'overflow': 'hidden', 'margin-right': '17px'});
			dataAttr = $(this).attr('data-open');
			$('[data-id="' + dataAttr + '"]').addClass('visible');
		})
		$('[data-close]').click(function() {
			$('body').css({'overflow': 'visible', 'margin-right': '0'});
			$('[data-id="' + dataAttr + '"]').removeClass('visible');
		})
		$('[data-toggle="modal"]').click(function() {
			$('[data-close]').trigger('click');
		})
	}
	function calcScale() {
		var scale = $('[data-scale-id="scale"]'),
			parent = scale.parents('.percent-block');
		scale.each(function() {
			var current = $(this).attr('data-scale-current'),
				descrCurpercent = $(this).closest('.percent-block').find('[data-scale-descr-curr-percent]'),
				calcedInPercent = current + '%';
			$(this).width( calcedInPercent );
			descrCurpercent.text( '('+ calcedInPercent+')' );
		})
	};
	win.on('load resize scroll', function(e) {
		if ( e.type == 'load' ) {
			dEnd = new Date();
			totalD = dEnd - dStart;
			setTimeout( function() {
				$(".loading").fadeOut();
			}, totalD);
		}
		if ( e.type == 'resize' ) {
			waitForFinalEvent(function() {
				clearClasses();
			}, 500, "resize")
		}
		if ( e.type == 'scroll' ) {
			scrollT = $(this).scrollTop();
		}
	})

	doc.on('click', function(e) {
		if ( e.type == 'click' ) {
			var target = $(e.target);
		}
	})
	doc.ready(function() {
		mobileFullPageToggle();
		calcScale();
	})
}())