module.exports = function () {
    $.gulp.task('bowerJs', function() {
		return $.gulp.src($.mainFiles('**/*.js'))
			.pipe($.uglify())
			.pipe($.gulp.dest('./build/js/libs/'))
		  	.pipe($.debug({ "title": "bowerJs" }))
		  	.on("end", $.bs.reload);
	});
};